## References

- https://learn.hashicorp.com/terraform/getting-started/install.html
- https://www.terraform.io/docs/providers/google/getting_started.html
- https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform

## Quick Howto

* Install Terraform & configure GCP provider
* Clone this project
* Provision CloudSQL & get its public ip
* `cd 01-database`
* `terraform init`
* `terraform plan`
* `terraform apply`
* Copy output CloudSQL public ip
* `cd 02-appserver`
* `vim script/myconnect.php` (paste output cloudsql public ip)
* `terraform init`
* `terraform plan`
* `terraform apply`
* Take a snapshot of master vm for Autoscaler by stopping & re-start the vm instance
* `gcloud compute instances stop master-appserver`
* `gcloud compute --project=ca-mirasz-test images create master-appserver-image --source-disk=master-appserver --source-disk-zone=asia-southeast1-a`
* `gcloud compute instances start master-appserver`
* Create Templates & provision Managed Instance Group
* Rename all `.tf.disable` files to `.tf`
* terraform plan
* terraform apply

## Load Balancer & Monitoring

Load balancer & monitoring are provisioned manually via the Console.
Important url:
- homepage: http://35.244.227.242/
- php info: http://35.244.227.242/info.php
- db conn.: http://35.244.227.242/myconnect.php

## CloudSQL Proxy

- `prompt1> curl -o cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.darwin.amd64`
- `prompt1> chmod +x cloud_sql_proxy`
- `prompt1> ./cloud_sql_proxy -instances=ca-mirasz-test:asia-southeast1:mydb-instance-5c75ceaa16539d49=tcp:3306`

```
prompt2> mysql -u root2 -p -h127.0.0.1
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 344
Server version: 5.7.14-google (Google)

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> exit
Bye
```

## SFTP/SSH Access

- Private key for master instance are here: `02-appserver/keys/ubuntu_rsa`
- `ssh -i keys/ubuntu_rsa ubuntu@35.247.128.201 -o StrictHostKeyChecking=no`
- SFTP access can be achieved by generating new private key from gcloud console
