// Terraform plugin for creating random ids
resource "random_id" "mydb-instance" {
 byte_length = 8
}

resource "google_sql_database_instance" "mydb-instance" {
  name = "mydb-instance-${random_id.mydb-instance.hex}"
  database_version = "MYSQL_5_7"
  region = "asia-southeast1"

  settings {
    tier = "db-n1-standard-1"
    ip_configuration {
      authorized_networks {
        value = "0.0.0.0/0"
        name = "allow from any"
      }
    }
  }
}

resource "google_sql_user" "users" {
  name = "root2"
  instance = "${google_sql_database_instance.mydb-instance.name}"
  host = "%"
  password = "think2"
}

output "ip-cloudsql" {
  value       = "${google_sql_database_instance.mydb-instance.ip_address.0.ip_address}"
  description = "Please copy & paste the ip address to myconnect.php file"
}

