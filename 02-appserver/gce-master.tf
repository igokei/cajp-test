// Reference:
// https://cloud.google.com/community/tutorials/getting-started-on-gcp-with-terraform

// A single Google Cloud Engine instance
resource "google_compute_instance" "web-ap-master" {
 name         = "master-appserver"
 machine_type = "n1-standard-1"
 zone         = "${var.SEA_REGION_A}"

 boot_disk {
   initialize_params {
     image = "ubuntu-os-cloud/ubuntu-1804-lts"
   }
 }

// Install http service & php
 metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync apache2 libapache2-mod-php php7.2-mysql php7.2-cli mysql-client-core-5.7; sudo systemctl restart apache2"

 metadata {
   sshKeys = "ubuntu:${file("./keys/appserver_rsa.pub")}"
 }

 network_interface {
   //network = "${google_compute_network.myvpc.name}"
   subnetwork = "${google_compute_subnetwork.sea_mysubnet.name}"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
//
 provisioner "file" {
   source = "./script/setup.sh"
   destination = "setup.sh"
   connection {
     type = "ssh"
     user = "ubuntu"
     private_key = "${file("./keys/appserver_rsa")}"
   }
 }
 provisioner "file" {
   source = "./script/info.php"
   destination = "info.php"
   connection {
     type = "ssh"
     user = "ubuntu"
     private_key = "${file("./keys/appserver_rsa")}"
   }
 }
 provisioner "file" {
   source = "./script/myconnect.php"
   destination = "myconnect.php"
   connection {
     type = "ssh"
     user = "ubuntu"
     private_key = "${file("./keys/appserver_rsa")}"
   }
 }

 provisioner "remote-exec" {
   inline = [
     "sleep 90",
     "chmod +x ./setup.sh",
     "./setup.sh"
   ]
   connection {
     type = "ssh"
     user = "ubuntu"
     private_key = "${file("./keys/appserver_rsa")}"
   }
 }
//
}

// A variable for extracting the external ip of the instance
output "ip-ap-master" {
 value = "${google_compute_instance.web-ap-master.network_interface.0.access_config.0.nat_ip}"
}
