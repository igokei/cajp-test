variable "SEA_REGION_A" {
  default = "asia-southeast1-a"
}

variable "SEA_REGION_B" {
  default = "asia-southeast1-b"
}

variable "NEA_REGION_A" {
  default = "asia-northeast1-a"
}

variable "NEA_REGION_B" {
  default = "asia-northeast1-b"
}
