// SEA
resource "google_compute_firewall" "sea-myfirewall-icmp" {
 name    = "sea-myfirewall-icmp"
 network = "${google_compute_network.sea_myvpc.name}"

 allow {
   protocol = "icmp"
 }

 source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "sea-myfirewall-ssh" {
 name    = "sea-myfirewall-ssh"
 network = "${google_compute_network.sea_myvpc.name}"

 allow {
   protocol = "tcp"
   ports    = ["22"]
 }

 source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "sea-myfirewall-http" {
 name    = "sea-myfirewall-http"
 network = "${google_compute_network.sea_myvpc.name}"

 allow {
   protocol = "tcp"
   ports    = ["80"]
 }

 source_ranges = ["0.0.0.0/0"]
}

// NEA
resource "google_compute_firewall" "nea-myfirewall-icmp" {
 name    = "nea-myfirewall-icmp"
 network = "${google_compute_network.nea_myvpc.name}"

 allow {
   protocol = "icmp"
 }

 source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "nea-myfirewall-ssh" {
 name    = "nea-myfirewall-ssh"
 network = "${google_compute_network.nea_myvpc.name}"

 allow {
   protocol = "tcp"
   ports    = ["22"]
 }

 source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "nea-myfirewall-http" {
 name    = "nea-myfirewall-http"
 network = "${google_compute_network.nea_myvpc.name}"

 allow {
   protocol = "tcp"
   ports    = ["80"]
 }

 source_ranges = ["0.0.0.0/0"]
}
