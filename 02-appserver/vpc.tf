// Terraform plugin for creating random ids
resource "random_id" "sea_myvpc" {
 byte_length = 2
}

resource "random_id" "nea_myvpc" {
 byte_length = 2
}

// VPC SEA
resource "google_compute_network" "sea_myvpc" {
 name = "sea-myvpc-${random_id.sea_myvpc.hex}"
 auto_create_subnetworks = "false"
}

// Subnet SEA
resource "google_compute_subnetwork" "sea_mysubnet" {
 name = "sea-mysubnet-${random_id.sea_myvpc.hex}"
 ip_cidr_range = "172.16.1.0/24"
 network = "sea-myvpc-${random_id.sea_myvpc.hex}"
 depends_on = ["google_compute_network.sea_myvpc"]
 region = "asia-southeast1"
}

// VPC NEA
resource "google_compute_network" "nea_myvpc" {
 name = "nea-myvpc-${random_id.nea_myvpc.hex}"
 auto_create_subnetworks = "false"
}

// Subnet NEA
resource "google_compute_subnetwork" "nea_mysubnet" {
 name = "nea-mysubnet-${random_id.nea_myvpc.hex}"
 ip_cidr_range = "172.16.2.0/24"
 network = "nea-myvpc-${random_id.nea_myvpc.hex}"
 depends_on = ["google_compute_network.nea_myvpc"]
 region = "asia-northeast1"
}
