// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("./credentials/ca-mirasz-test-993311341063.json")}"
 project     = "ca-mirasz-test"
 region      = "asia-southeast1-a"
}
